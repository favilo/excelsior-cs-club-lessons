     ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--, 
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    ----------------------------------------------------------------- 


Hi there! Welcome to the CS Club for Excelsior Charter Schools - Ontario

To get you started, we included a few samples and lessons

1) Open the class folder and start with lesson01

2) Open index.html

3) Click on the Preview button to open a live preview pane

4) Make some changes to the file, save, watch the preview, and have fun!
   We've included a test suite to make sure you've done everything exactly like
   the directions say.

Have fun learning to code!

The Excelsior Ontario CS Club teachers
