QUnit.test( "hello test", function( assert ) {
  assert.ok( 1 == "1", "This is what a passing test looks like" );
});

QUnit.test( "output1 test", function( assert ) {
  assert.ok( document.getElementById("output1").innerHTML == "Something Awesome!", "Replace paragraph with \"Something Awesome!\"" );
});

QUnit.test( "output2 test", function( assert ) {
  assert.ok( document.getElementById("output2").innerHTML == 5, "Replace paragraph with 5" );
});

var carName;
QUnit.test( "carName test", function( assert ) {
  assert.ok(carName == "Honda", "Create a variable called carName and assign \"Honda\" to it");
});